package no.ntnu.item.ttm3.hns.heatergui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.internal.InternalMessage;
import no.ntnu.item.ttm3.hns.domain.internal.device.TemperaturePayload;

public class HeaterGUI extends Block {

	JButton register;
	JButton deregister;
	JLabel label;
	JFrame frame;

	public void init(String deviceName) {

		frame = new JFrame(deviceName);
		register = new JButton("Plug In");
		deregister = new JButton("Plug Out");
		
		final GridBagConstraints c = new GridBagConstraints();
		final GridBagConstraints gbc = new GridBagConstraints();
		final JPanel panel = new JPanel();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.setLocation(300, 0);
				frame.getContentPane().setLayout(new GridBagLayout());
				deregister.setEnabled(false);
			}
		});

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 3;
		c.insets = new Insets(10, 10, 10, 10);
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 10, 10);
		
		register.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				sendToBlock("REGISTER");
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HeaterGUI.this.register.setEnabled(false);
						HeaterGUI.this.deregister.setEnabled(true);
					}
				});
			}
		});
		
		deregister.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sendToBlock("DEREGISTER");
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HeaterGUI.this.register.setEnabled(true);
						HeaterGUI.this.deregister.setEnabled(false);
					}
				});
			}
		});
		
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				sendToBlock("EXIT"); 
			}
		});
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.getContentPane().add(register, c);
				c.gridy = 1;
				frame.getContentPane().add(deregister, c);
				c.gridy =2;
				
				panel.setSize(160,20);
				panel.setLayout(new GridBagLayout());
				panel.setBorder(BorderFactory.createTitledBorder("Temperature"));
				
				label = new JLabel("                                   ");
				label.setSize(160,20);
				label.setBackground(new Color(0, 0, 255));
				label.setOpaque(true);
				panel.add(label, gbc);
				panel.revalidate();		
				frame.getContentPane().add(panel, c);
				frame.revalidate();
				
				frame.setSize(200,200);
				frame.setVisible(true);
			}
		});

	}

	private void guiUpdateTemperature(int temperature) {
		label.setBackground(new Color(temperature, 0, 255-temperature));
		label.setOpaque(true);
		frame.revalidate();
	}

	public void updateFromLogic(InternalMessage message) {
		if(message.getPayload().getType().equals(TemperaturePayload.class.getName())) {
			TemperaturePayload payload = (TemperaturePayload)message.getPayload();
			guiUpdateTemperature(payload.getTemp());
		} 
	}

}
