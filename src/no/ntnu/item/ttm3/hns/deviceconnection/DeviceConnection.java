package no.ntnu.item.ttm3.hns.deviceconnection;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.external.Address;
import no.ntnu.item.ttm3.hns.domain.external.ExternalMessage;
import no.ntnu.item.ttm3.hns.router.Router;

public class DeviceConnection extends Block {
	
	public no.ntnu.item.ttm3.hns.domain.external.Address localAddress;

	public void registerDevice() {
		Router.register(localAddress.getSessionID(), new Router.MessageListener() {
			@Override
			public void receive(ExternalMessage message) {
				sendToBlock("RECEIVE", message);
			}
		});
	}
	
	public void deregisterDevice() {
		ExternalMessage message = new ExternalMessage("Deregister Device");
		send(message);
		Router.deregister(localAddress.getSessionID());
	}

	public Address generateLocalAddress(String deviceName) {
		Address localAddress = Router.localAddress.getCopy();
		localAddress.setSessionID(deviceName);
		return localAddress;
	}

	public ExternalMessage receivedMessage(ExternalMessage message) {
		System.err.println("["+localAddress.getSessionID()+"] received: "+message.getSignalID());
		return message;
	}

	public void send(ExternalMessage message) {
		message.setSender(localAddress);
		Router.send(message);
	}
	
}