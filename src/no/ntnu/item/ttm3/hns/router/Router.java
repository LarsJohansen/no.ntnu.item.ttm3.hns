package no.ntnu.item.ttm3.hns.router;

import java.util.HashMap;
import java.util.Map;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.external.Address;
import no.ntnu.item.ttm3.hns.domain.external.ExternalMessage;

public class Router extends Block {

	static Router instance;

	public static interface MessageListener {
		public void receive(ExternalMessage message);
	}

	private static Map<String, MessageListener> listeners = new HashMap<String, Router.MessageListener>();
	// Instance parameter. Edit only in overview page.
	public final java.lang.String type;
	public no.ntnu.item.ttm3.hns.domain.external.ExternalMessage message;
	public static no.ntnu.item.ttm3.hns.domain.external.Address localAddress;
	
	public static final void register(String sessionID, MessageListener listener) {
		if (!listeners.containsKey(sessionID)) {
			System.err.println("register listener on " + sessionID);
			listeners.put(sessionID, listener);
		}
	}

	public static final void send(ExternalMessage message) {
		assert instance != null;
		assert message.getSender() != null;
		instance.sendToBlock("OUTBOUND", message);
	}

	public void receive(ExternalMessage message) {
		// If a specific receiver has been set for the message
		if (message.getReceiver() != null) {
			MessageListener listener = listeners.get(message.getReceiver()
					.getSessionID());
			if (listener != null) {
				listener.receive(message);
			} else {
				System.err.println("Message discarded!" + message.getSignalID()
						+ " " + message.getReceiver().getSessionID());
			}
		} 
		// If a message should be broadcasted
		else {
			for (Map.Entry<String, Router.MessageListener> entry : listeners.entrySet()) {
				String sessionID = entry.getKey();
				if (sessionID.equals(message.getSender().getSessionID())) // Don't broadcast to self
					continue;
				MessageListener listener = listeners.get(sessionID);
				if (listener != null) {
					listener.receive(message);
				} else {
					System.err.println("Message discarded!" + message.getSignalID()
							+ " " + message.getReceiver().getSessionID());
				}
			}
		}
	}

	public static void deregister(String sessionID) {
		listeners.remove(sessionID);
	}

	public void stop() {
		listeners.clear();
	}

	// Do not edit this constructor.
	public Router(java.lang.String type) {
		this.type = type;
		instance = this;
	}

	public void setLocalAddress() {
		Address local = new Address();
		local.setPort("52000");
		local.setIP("localhost");
		localAddress = local;
	}

}
