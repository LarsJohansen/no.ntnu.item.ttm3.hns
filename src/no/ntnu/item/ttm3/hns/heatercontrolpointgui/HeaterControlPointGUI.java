package no.ntnu.item.ttm3.hns.heatercontrolpointgui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.external.Address;
import no.ntnu.item.ttm3.hns.domain.internal.InternalMessage;
import no.ntnu.item.ttm3.hns.domain.internal.controller.DeregisterDevicePayload;
import no.ntnu.item.ttm3.hns.domain.internal.controller.RegisterDevicePayload;
import no.ntnu.item.ttm3.hns.domain.internal.controller.TemperaturePayload;

public class HeaterControlPointGUI extends Block {

	private JButton register;
	private JButton deregister;
	private GridBagConstraints c;
	private JFrame frame;

	public void init(final String deviceName) {
		
		frame = new JFrame(deviceName);
		c = new GridBagConstraints();
		register = new JButton("Plug In");
		deregister = new JButton("Plug Out");
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.setLocation(0, 0);
				deregister.setEnabled(false);
				frame.getContentPane().setLayout(new GridBagLayout());
			}
		});
		
		register.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sendToBlock("REGISTER");
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HeaterControlPointGUI.this.register.setEnabled(false);
						HeaterControlPointGUI.this.deregister.setEnabled(true);
					}
				});
			}
		});

		deregister.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final ArrayList<JPanel> components = new ArrayList<JPanel>();
				for (Component component : frame.getContentPane().getComponents()) {
					if (component instanceof JPanel) {
						JPanel panel = (JPanel) component;
						components.add(panel);
					}
				}
				for (final JPanel panel : components) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							frame.getContentPane().remove(panel);
							frame.revalidate();
							frame.repaint();
						}
					});
				}
				sendToBlock("DEREGISTER");
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HeaterControlPointGUI.this.register.setEnabled(true);
						HeaterControlPointGUI.this.deregister.setEnabled(false);
					}
				});
			}
		});

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				sendToBlock("EXIT");
			}
		});

		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(10, 10, 10, 10);

		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.getContentPane().add(register, c);
				c.gridy = 1;
				frame.getContentPane().add(deregister, c);
				
				frame.setSize(300, 350);
				frame.setVisible(true);
			}
		});

	}

	private void guiRegisterDevice(final Address in) {

		final JPanel panel = new JPanel();
		JSlider slider = new JSlider(0,255, 0);
		
		slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider)e.getSource();
				TemperaturePayload payload = new TemperaturePayload(in.getSessionID());
				payload.setTemperature(source.getValue());
				InternalMessage message = new InternalMessage(payload);
				sendToBlock("SET_TEMP", message);
			}
		});

		c.weighty = 3;
		c.gridy += 1;
		frame.getContentPane().add(panel, c);
		panel.setLayout(new GridBagLayout());
		panel.setName(in.getSessionID());
		panel.setBorder(BorderFactory.createTitledBorder("Controlling: "+in.getSessionID()));

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weighty = 1.0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 10, 10, 10);
		panel.add(slider, gbc);
		frame.revalidate();
		frame.repaint();
	}

	private void guiDeregisterDevice(String sessionID) {
		for (Component comp : frame.getContentPane().getComponents()) {
			if(comp instanceof JPanel && ((JPanel)comp).getName().equals(sessionID)) {
				frame.getContentPane().remove(comp);
				frame.revalidate();
				frame.repaint();
				return;
			}
		}
	}

	public void updateFromLogic(InternalMessage message) {
	
		if(message.getPayload().getType().equals(RegisterDevicePayload.class.getName())) {
			RegisterDevicePayload payload = (RegisterDevicePayload)message.getPayload();
			guiRegisterDevice(payload.getAddress());
		} 
		else if(message.getPayload().getType().equals(DeregisterDevicePayload.class.getName())) {
			DeregisterDevicePayload payload = (DeregisterDevicePayload)message.getPayload();
			guiDeregisterDevice(payload.getSessionID());
		}
	}
}
