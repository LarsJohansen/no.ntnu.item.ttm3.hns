package no.ntnu.item.ttm3.hns.domain;

public interface Payload {
	
	public String getType();

}
