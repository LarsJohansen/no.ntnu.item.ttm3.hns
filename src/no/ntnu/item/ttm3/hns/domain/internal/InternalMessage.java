package no.ntnu.item.ttm3.hns.domain.internal;

import no.ntnu.item.ttm3.hns.domain.Payload;

public class InternalMessage {
	
	private Payload payload;
	
	public InternalMessage(Payload payload) {
		this.payload = payload;
	}

	public Payload getPayload() {
		return payload;
	}

}
