package no.ntnu.item.ttm3.hns.domain.internal.controller;

import no.ntnu.item.ttm3.hns.domain.Payload;

public class DeregisterDevicePayload implements Payload {
	
	private String sessionID;
	
	public DeregisterDevicePayload(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getSessionID() {
		return sessionID;
	}
	
	public String getType() {
		return DeregisterDevicePayload.class.getName();
	}

}
