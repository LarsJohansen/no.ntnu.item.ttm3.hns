package no.ntnu.item.ttm3.hns.domain.internal.controller;

import no.ntnu.item.ttm3.hns.domain.Payload;

public class TemperaturePayload implements Payload {
	
	private int temperature;
	private String sessionID;
	
	public TemperaturePayload(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTemperature(int temperature){
		this.temperature = temperature;
	}
	
	public int getTemp(){
		return temperature;
	}
	
	public void setSessionID(String string){
		this.sessionID = string;
	}
	
	public String getSessionID(){
		return sessionID;
	}

	@Override
	public String getType() {
		return TemperaturePayload.class.getName();
	}

}
