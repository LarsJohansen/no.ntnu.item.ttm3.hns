package no.ntnu.item.ttm3.hns.domain.internal.device;

import no.ntnu.item.ttm3.hns.domain.Payload;

public class TemperaturePayload implements Payload {
	
	private int temperature;
	
	public TemperaturePayload(int temperature) {
		this.temperature = temperature;
	}
	
	public int getTemp(){
		return temperature;
	}

	@Override
	public String getType() {
		return TemperaturePayload.class.getName();
	}

}
