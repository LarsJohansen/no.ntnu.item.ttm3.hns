package no.ntnu.item.ttm3.hns.domain.internal.controller;

import no.ntnu.item.ttm3.hns.domain.Payload;
import no.ntnu.item.ttm3.hns.domain.external.Address;

public class RegisterDevicePayload implements Payload {

	private Address address;
	
	public RegisterDevicePayload(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}
	
	public String getType() {
		return RegisterDevicePayload.class.getName();
	}

}
