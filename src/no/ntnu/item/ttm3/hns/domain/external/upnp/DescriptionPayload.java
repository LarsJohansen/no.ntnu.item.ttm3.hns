package no.ntnu.item.ttm3.hns.domain.external.upnp;

import no.ntnu.item.ttm3.hns.domain.external.ExternalPayload;

/**
 * Currently not in use
 */
public class DescriptionPayload implements ExternalPayload {

	public String getType() {
		return DescriptionPayload.class.getName();
	}

	public String getSessionID() {
		return null;
	}

	public void setSessionID(String sessionID) {
	}
	
}
