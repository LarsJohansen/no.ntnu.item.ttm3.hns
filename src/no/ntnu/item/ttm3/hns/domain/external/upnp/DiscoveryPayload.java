package no.ntnu.item.ttm3.hns.domain.external.upnp;

import no.ntnu.item.ttm3.hns.domain.external.ExternalPayload;

/**
 * Currently not in use
 */
public class DiscoveryPayload implements ExternalPayload {

	public String getType() {
		return DiscoveryPayload.class.getName();
	}

	@Override
	public String getSessionID() {
		return null;
	}

	@Override
	public void setSessionID(String sessionID) {		
	}
	
}
