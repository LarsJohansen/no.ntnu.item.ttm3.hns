package no.ntnu.item.ttm3.hns.domain.external.upnp.heater;

import no.ntnu.item.ttm3.hns.domain.external.upnp.ControlPayload;

public class HeaterControlPayload implements ControlPayload {

	private int temperature;
	private String sessionID;
	
	public HeaterControlPayload(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTemperature(int temperature){
		this.temperature = temperature;
	}
	
	public int getTemp(){
		return temperature;
	}
	
	public void setSessionID(String string){
		this.sessionID = string;
	}
	
	public String getSessionID(){
		return sessionID;
	}

	public String getType() {
		return HeaterControlPayload.class.getName();
	}
	
}
