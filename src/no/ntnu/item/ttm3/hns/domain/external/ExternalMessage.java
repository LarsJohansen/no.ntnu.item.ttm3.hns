package no.ntnu.item.ttm3.hns.domain.external;

import no.ntnu.item.ttm3.hns.domain.Payload;


public class ExternalMessage {
	
	private String signalID;
	
	private Address sender, receiver;
	
	private Payload payload;
	
	public ExternalMessage(String signalID) {
		this.signalID = signalID;
	}
	
	public String getSignalID() {
		return signalID;
	}

	/**
	 * ID of the signal, consisting of lower case letters, numbers and "_".
	 * 
	 * @param signalID
	 */
	public void setSignalID(String signalID) {
		this.signalID = signalID;
	}

	public Address getSender() {
		return sender;
	}

	public void setSender(Address sender) {
		this.sender = sender.getCopy();
	}

	public Address getReceiver() {
		return receiver;
	}

	public void setReceiver(Address receiver) {
		assert receiver!=null;
		assert this.receiver==null;
		this.receiver = receiver.getCopy();
	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

}
