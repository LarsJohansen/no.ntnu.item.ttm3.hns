package no.ntnu.item.ttm3.hns.domain.external;

import no.ntnu.item.ttm3.hns.domain.Payload;

public interface ExternalPayload extends Payload {

	/**
	 * Returns the session ID for the receiver
	 * @return Receivers session ID
	 */
	public String getSessionID();
	
	/**
	 * Sets the receivers session ID
	 */
	public void setSessionID(String sessionID);	
	
}
