package no.ntnu.item.ttm3.hns.heatercontrolpointlogic;

import java.util.HashMap;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.external.Address;
import no.ntnu.item.ttm3.hns.domain.external.ExternalMessage;
import no.ntnu.item.ttm3.hns.domain.external.upnp.DescriptionPayload;
import no.ntnu.item.ttm3.hns.domain.external.upnp.DiscoveryPayload;
import no.ntnu.item.ttm3.hns.domain.external.upnp.heater.HeaterControlPayload;
import no.ntnu.item.ttm3.hns.domain.internal.InternalMessage;
import no.ntnu.item.ttm3.hns.domain.internal.controller.DeregisterDevicePayload;
import no.ntnu.item.ttm3.hns.domain.internal.controller.RegisterDevicePayload;
import no.ntnu.item.ttm3.hns.domain.internal.controller.TemperaturePayload;

public class HeaterControlPointLogic extends Block {

	public no.ntnu.item.ttm3.hns.domain.external.ExternalMessage receivedMessage;
	public java.util.Map<String, Address> devices;

	public void init() {
		devices= new HashMap<String,Address>();
	}

	public String extractSignalID() {
		return receivedMessage.getSignalID();
	}
	
	public void unexpectedSignal() {
	}

	public InternalMessage descriptionHeaterResp(ExternalMessage in) {
		// Use info in the description payload
		devices.put(in.getSender().getSessionID(), in.getSender());
		RegisterDevicePayload payload = new RegisterDevicePayload(in.getSender());
		return new InternalMessage(payload);
	}
	
	public InternalMessage deregisterDevice(ExternalMessage in) {
		devices.remove(in.getSender().getSessionID());
		
		DeregisterDevicePayload payload = new DeregisterDevicePayload(in.getSender().getSessionID());
		return new InternalMessage(payload);
	}
	
	public ExternalMessage broadcastDiscoverHeaterReq() {
		ExternalMessage message = new ExternalMessage("CPDiscover Heater Req");
		DiscoveryPayload payload = new DiscoveryPayload();
		// Add info to discovery payload if needed
		message.setPayload(payload);
		return message;
	}

	public ExternalMessage discoverHeaterResp(ExternalMessage in) {
		// Use info in the discovery payload response
		ExternalMessage response = new ExternalMessage("CPDescription Heater Req");
		DescriptionPayload payload = new DescriptionPayload();
		response.setPayload(payload);
		response.setReceiver(in.getSender());
		return response;
	}
	
	public ExternalMessage discoverHeaterReq(ExternalMessage in) {
		// Use info in the discovery payload request
		ExternalMessage message = new ExternalMessage("CPDiscover Heater Req");
		message.setReceiver(in.getSender());
		return message;
	}
	
	
	public ExternalMessage updateFromGui(InternalMessage message) {
		if(message.getPayload().getType().equals(TemperaturePayload.class.getName())) {
			TemperaturePayload tempPayload = (TemperaturePayload)message.getPayload();
			
			HeaterControlPayload payload = new HeaterControlPayload(tempPayload.getSessionID());
			payload.setTemperature(tempPayload.getTemp());
			
			ExternalMessage externalMessage = new ExternalMessage("CPSet Temperature");
			externalMessage.setPayload(payload);
			externalMessage.setReceiver(devices.get(payload.getSessionID()));
			return externalMessage;
		}
		assert(false);
		return null;
	}
}