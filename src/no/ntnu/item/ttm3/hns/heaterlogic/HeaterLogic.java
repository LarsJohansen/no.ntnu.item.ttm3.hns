package no.ntnu.item.ttm3.hns.heaterlogic;

import no.ntnu.item.arctis.runtime.Block;
import no.ntnu.item.ttm3.hns.domain.external.ExternalMessage;
import no.ntnu.item.ttm3.hns.domain.external.upnp.DescriptionPayload;
import no.ntnu.item.ttm3.hns.domain.external.upnp.DiscoveryPayload;
import no.ntnu.item.ttm3.hns.domain.external.upnp.heater.HeaterControlPayload;
import no.ntnu.item.ttm3.hns.domain.internal.InternalMessage;
import no.ntnu.item.ttm3.hns.domain.internal.device.TemperaturePayload;

public class HeaterLogic extends Block {

	public int temp;
	public no.ntnu.item.ttm3.hns.domain.external.ExternalMessage receivedMessage;
	public String extractSignalID() {
		return receivedMessage.getSignalID();
	}
	
	public void unexpectedSignal() {
	}

	public ExternalMessage cpDiscoverHeaterReq(ExternalMessage in) {
		ExternalMessage response = new ExternalMessage("Discover Heater Resp");
		DiscoveryPayload payload = new DiscoveryPayload();
		// Add discovery info to the payload
		response.setPayload(payload);
		response.setReceiver(in.getSender());
		return response;
	}

	public ExternalMessage cpDescriptionHeaterReq(ExternalMessage in) {
		ExternalMessage response = new ExternalMessage("Description Heater Resp");
		response.setReceiver(in.getSender());
		DescriptionPayload payload = new DescriptionPayload();
		// Add description to the payload
		response.setPayload(payload);
		return response;
	}
	
	public ExternalMessage discoverDevices() {
		ExternalMessage message = new ExternalMessage("Discover Heater Req");
		DiscoveryPayload payload = new DiscoveryPayload();
		// Add discovery info to the payload
		message.setPayload(payload);
		return message;
	}
	
	public void init() {
	}

	public InternalMessage setTemperature(ExternalMessage msg) {
		if(msg.getPayload().getType().equals(HeaterControlPayload.class.getName())) {
			int temperature = ((HeaterControlPayload) msg.getPayload()).getTemp();
			TemperaturePayload payload = new TemperaturePayload(temperature);
			return new InternalMessage(payload);
		}
		return null;
	}
}